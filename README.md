# README #

React with Webpack (and gulp). See http://aergonaut.com/rails-4-gulp-webpack-react/ for initial setup.

To install all javascript modules, run:

    npm install

To globally install the webpack and gulp commands:

    sudo npm install -g gulp webpack


When developing, run this command to watch javascript files and recompile on change:

    gulp

This is the same as `gulp watch`. See `gulpfile.js`.

Tip: You can use `foreman` with a `Procfile` to execute `gulp` and `rails s` with one command.

Gotcha: When adding new JS files, restart gulp to pick up changes in the new files.

The Webpack configuration can be found in `config/webpack.config.js`.


The entry point for all javascript is `entry.js`. This file can use `require()` instructions to
include more javascript code. The output is compiled to `bundle.js`. Only `bundle.js` should be served.

If you need more modules from the interwebs, use:

    npm install --save new_package

to install the `new_package` module.

# EXAMPLES #

TodoApp - A simple todo list with add and remove
RefluxTodoApp - The same todo list but with Reflux store and actions
HighchartsBar - Render a chart with the Highcharts library, very simple POC
SelectTestApp - A select box element (not an app...)
