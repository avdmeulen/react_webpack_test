var Reflux = require('reflux');

var ProductActions = Reflux.createAction({
  "load": { children: ["completed", "failed"] }
});

module.exports = ProductActions;