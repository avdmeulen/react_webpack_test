var Reflux = require('reflux');

var SelectionActions = Reflux.createActions([
  'updateSelection',
  'resetSelection'
]);

module.exports = SelectionActions;