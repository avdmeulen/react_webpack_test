var Reflux = require('reflux');

var FinancialActions = Reflux.createAction({
  "fetchData": { children: ["completed", "failed"] }
});

module.exports = FinancialActions;