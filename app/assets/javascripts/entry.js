// This file is the entry point for all javascript that is compiled with webpack.

console.log('Hoi');

var React = require('react/addons'),
    TodoApp = React.createFactory(require('./components/TodoApp')),
    RefluxTodoApp = React.createFactory(require('./components/RefluxTodoApp')),
    SelectTestApp = React.createElement(require('./components/SelectTestApp')),
    HighchartsBar = React.createFactory(require('./components/HighchartsBar')),
    IndicatorApp = React.createFactory(require('./components/IndicatorApp')),
    FinancialStore = require('./stores/FakeFinancialStore');

window.initTodosApp = function(state, rootNode) {
  React.render(
    RefluxTodoApp(state),
    rootNode
  );
}

window.initSelectTestApp = function(rootNode) {
  React.render(
    SelectTestApp,
    rootNode
  )
}

window.initHighchartsBar = function(state, rootNode) {
  React.render(
    HighchartsBar(state),
    rootNode
  )
}

window.initIndicatorApp = function(state, rootNode) {
  React.render(
    IndicatorApp(state),
    rootNode
  )
}

window.FinancialStore = FinancialStore;
