// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
// This app uses the Webpack javascript pipeline.
// The entry point entry.js is compiled to bundle.js
// See README for more info.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require highcharts
//= require handsontable.full
//= require bundle
