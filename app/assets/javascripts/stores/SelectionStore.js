var Reflux = require('reflux');
var SelectionActions = require('../actions/SelectionActions');
var FinancialActions = require('../actions/FinancialActions');

var SelectionStore = Reflux.createStore({
  listenables: [SelectionActions],

  onResetSelection: function() {
    this.UpdateSelection(this.getInitialState());
  },

  onUpdateSelection: function(selection) {
    this.updateSelection(_.merge(this.selection, selection));
  },

  updateSelection: function(selection) {
    console.log('SelectionStore::updateSelection')
    this.trigger(this.selection);
    // Also trigger an update of the financial store
    FinancialActions(selection);
  },

  getInitialState: function() {
    this.selection = {
      noMonths: 36,
      monthType: 'm',
      dimension: 'sources',
      elements: {
        sources: 'all',
        departments: '1',
        projects: '1',
        products: '1',
        relations: '1'
      }
    };
    return this.selection;
  }

});

module.exports = SelectionStore;