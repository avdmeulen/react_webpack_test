var Reflux = require('reflux');
var FinancialActions = require('../actions/FinancialActions');

var availableSources = [
  { id: 'all', name: 'Overzicht' },
  { id: 'realisation', name: 'Realisatie' },
  { id: 'forecast', name: 'Forecast' },
  { id: 'ambition', name: 'Ambitie' }
];

var availableDummies = [
  { id: '1', name: 'Alle' },
  { id: '2', name: 'Geen' },
  { id: '3', name: 'ABC' },
  { id: '4', name: 'XYZ' }
];

// Example settings:
// {
//   "startAt": "01-2014",
//   "endAt": "12-2016",
//   "noMonths": 36,
//   "monthType": "cc",
//   "dimension": "sources",
//   "elements": {
//     "sources": "all",
//     "departments": "1",
//     "projects": "1",
//     "products": "1",
//     "relations": "1"
//   }
// }

var accountingData = function(settings) {
  var data = [];
  var cumulative = 0;
  var baseline = Math.floor(Math.random() * 6) * 1000;
  for (var i = 0; i < settings.noMonths; i++) { 
    var value = baseline + Math.floor(Math.random() * 1500) - 750
    if (settings.monthType == 'm') {
      data.push(value);
    } else {
      cumulative = cumulative + value
      data.push(cumulative);
    }
  }

  return data;
};

var generateBogusData = function(settings) {
  var sources = [];
  if (settings.elements.sources === 'all') {
    sources = ['realisation', 'forecast', 'ambition'];
  } else {
    sources.push(settings.elements.sources);
  }

  var data = [];
  for (var i = 0; i < sources.length; i++) {
    data.push({name: sources[i], data: accountingData(settings)});
  };

  return data;
};

var FakeFinancialStore = Reflux.createStore({
  init: function() {
    this.listenTo(FinancialActions, 'onFetchData');
  },

  getInitialState: function() {
    this.series = generateBogusData({noMonths: 36, elements: { source: 'all' }});
    return this.series;
  },

  onFetchData: function(settings) {
    this.series = generateBogusData(settings);
    console.log('New data generated', this.series);
    this.trigger(this.series);
  }
});

module.exports = FakeFinancialStore;