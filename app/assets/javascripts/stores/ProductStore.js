var Reflux = require('reflux');
var ProductActions = require('../actions/ProductActions');

var ProductStore = Reflux.createStore({
  // listenables: [ProductActions],

  init: function() {
    this.listenTo(ProductActions, 'onLoad');
  },

  onLoad: function() {
    var self = this;
    $.ajax({
      type: "get",
      url: "/products.json",
      dataType: 'json',
      // crossDomain: true,
      success: function(result) {
        self.onLoadCompleted(result);
      },
      error: function(error) {
        self.onLoadFailed(result);
      }
    });
  },

  onLoadCompleted: function(result) {
    console.log('AJAX Completed', result);
    this.updateList(result);
  },

  onLoadFailed: function(error) {
    console.log('AJAX Error', error);
  },

  updateList: function(list) {
    this.list = list;
    this.trigger(list);
  },

  getInitialState: function() {
    this.list = [];
    return this.list;
  }
});

module.exports = ProductStore;