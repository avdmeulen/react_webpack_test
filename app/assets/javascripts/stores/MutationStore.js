var Reflux = require('reflux');

var availableDummies = [
  { id: '1', name: 'Alle' },
  { id: '2', name: 'Geen' },
  { id: '3', name: 'ABC' },
  { id: '4', name: 'XYZ' }
];
    
var MutationStore = Reflux.createStore({
  
});

module.exports = MutationStore;