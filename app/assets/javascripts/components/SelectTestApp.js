var React = require('react/addons');
var Select = require('react-select');

var options = [
  { value: 'one', label: 'One' },
  { value: 'two', label: 'Two' }
];

var SelectTestApp = React.createClass({
  

  logChange: function(val) {
    console.log('logChange', val);
  },

  render: function() {
    return(
      <Select name="form-field-name" value="one" options={options} onChange={this.logChange} />
    );
  }

});

module.exports = SelectTestApp;