var React = require('react');

var TodoItem = React.createClass({
  // Can't use this.props.onDeletItem in the render function
  // because then it will only pass en event and not the element itself
  // TODO: fix that?
  handleClickDelete: function(e) {
    e.preventDefault();
    this.props.onDeleteItem(this);
  },

  render: function() {
    return <li>{this.props.itemText}<a href="#" onClick={this.handleClickDelete}>X</a></li>;
  }
});

var TodoList = React.createClass({
  render: function() {
    var self = this;
    var createItem = function(itemText, index) {
      return <TodoItem itemText={itemText} key={index} index={index} onDeleteItem={self.props.onDeleteItem} />;
    }
    return <ul>{this.props.items.map(createItem)}</ul>;
  }
});

var TodoApp = React.createClass({
  getInitialState: function() {
    return {items: this.props.items, text: this.props.text};
  },

  onChange: function(e) {
    this.setState({text: e.target.value});
  },

  handleSubmit: function(e) {
    e.preventDefault();
    var newText = this.state.text.trim();
    if (newText == '') return;
    var nextItems = this.state.items.concat([newText]);
    var nextText = '';
    this.setState({items: nextItems, text: nextText});
  },

  handleDeleteItem: function(e) {
    var nextItems = this.state.items.filter(function(itemText, index) {
      return index !== e.props.index;
    })
    this.setState({items: nextItems});
  },

  render: function() {
    return (
      <div>
        <h3>TODO</h3>
        <TodoList items={this.state.items} onDeleteItem={this.handleDeleteItem} />
        <form onSubmit={this.handleSubmit}>
          <input onChange={this.onChange} value={this.state.text} />
          <button>{'Add #' + (this.state.items.length + 1)}</button>
        </form>
      </div>
    );
  }
});

module.exports = TodoApp;