var React = require('react');
var DataTable = require('datatables-bootstrap3-plugin');

var accountingData = function() {
  var data = [];

  var random = (Math.floor(Math.random() * 100));

  for (var i = 0; i < random; i++) { 
    data.push({
      name: 'Transaction ' + (i + 1),
      amount: (Math.floor(Math.random() * 1000))
    });
  }

  return data;
};

var MutationsBox = React.createClass({

  getInitialState: function() {
    return {
      transactions: []
    };
  },

  componentDidMount: function() {
    $(this.refs.table.getDOMNode()).DataTable();
  },

  componentWillUpdate: function() {
    var table = $(this.refs.table.getDOMNode()).DataTable();
    table.destroy();
  },

  componentDidUpdate: function() {
    $(this.refs.table.getDOMNode()).DataTable();
  },

  componentWillUnmount: function() {
    var table = $(this.refs.table.getDOMNode()).DataTable();
    table.destroy();
  },

  render: function() {
    return (
      <div>
        <table ref="table" className="table">
          <thead>
            <th>Transaction Name</th>
            <th>Amount</th>
          </thead>
          <tbody>
            {
              this.state.transactions.map(function(trans, index) {
                return (
                  <tr key={index}>
                    <td>{trans.name}</td>
                    <td>{trans.amount}</td>
                  </tr>
                );
              })
            }        
          </tbody>
        </table>

        <a href="#" className="btn btn-default" onClick={this.handleGetTransactionsClick}>Get Transactions</a>
      </div>
    );
  },

  handleGetTransactionsClick: function(e) {
    e.preventDefault();

    this.setState({
      transactions: accountingData()
    });
  }

});

module.exports = MutationsBox;