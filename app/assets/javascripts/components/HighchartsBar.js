var React = require('react/addons');

var HighchartsBar = React.createClass({
  renderChart: function() {
    console.log('renderChart');
    var node = this.refs.chartNode.getDOMNode();
    var dataSeries = this.props.model;
    
    // use failsafe jQuery code using the $ alias, 
    // without relying on the global alias
    jQuery(function ($) {
      // remove chart if it was rendered before
      if ($(node).highcharts()) {
        $(node).highcharts().destroy();
      }

      // and add the chart
      $(node).highcharts({
        title: {
            text: 'Indicator Name',
            x: -20 //center
        },
        xAxis: {
            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        },
        yAxis: {
            title: {
                text: 'Euro'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: 'Euro'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: dataSeries //..rest of Highcharts code
      });
    });
 
  },

  getDefaultProps: function() {
    return {
      model: []
    };
  },

  componentWillReceiveProps: function(nextProps) {
    // we can use this method to see if the component is receiving props
  },

  shouldComponentUpdate: function(nextProps, nextState) {
    // should we update the component?
    console.log('shouldComponentUpdate', nextProps);
    return (typeof nextProps.model !== 'undefined') && nextProps.model.length > 0;
  },

  componentDidMount: function() {
    // render the chart when the component is mounted
    this.renderChart();
  },

  componentWillUnmount: function() {
    var node = this.refs.chartNode.getDOMNode();
    jQuery(node).highcharts().destroy();
  },

  componentDidUpdate: function() {
    // after the component props are updated, render the chart into the DOM node
    this.renderChart(); 
  },

  render: function() {
    return <div className="chart" ref="chartNode" />;
  }
});

module.exports = HighchartsBar;