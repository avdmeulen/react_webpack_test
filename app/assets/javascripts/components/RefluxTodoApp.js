var React = require('react');
var Reflux = require('reflux');

var actions = Reflux.createActions([
  'addItem',
  'removeItem'
]);

var store = Reflux.createStore({
  listenables: [actions],

  onAddItem: function(item) {
    this.updateList(this.list.concat([item]));
  },

  onRemoveItem: function(item) {
    this.updateList(_.filter(this.list, function(item2) {
      return item2 !== item;
    }));
  },

  updateList: function(list) {
    this.list = list;
    this.trigger(list);
  },

  getInitialState: function() {
    this.list = ['uit de store!'];
    return this.list;
  }
});

var TodoItem = React.createClass({
  handleClickDelete: function(e) {
    e.preventDefault();
    actions.removeItem(this.props.itemText);
  },

  render: function() {
    return <li>{this.props.itemText}<a href="#" onClick={this.handleClickDelete}>X</a></li>;
  }
});

var TodoList = React.createClass({
  render: function() {
    var self = this;
    var createItem = function(itemText, index) {
      return <TodoItem itemText={itemText} key={index} index={index} />;
    };
    return <ul>{this.props.items.map(createItem)}</ul>;
  }
});

var RefluxTodoApp = React.createClass({
  // Use the store as this.state.items
  mixins: [Reflux.connect(store, 'items')],

  getInitialState: function() {
    return {text: this.props.text};
  },

  onChange: function(e) {
    this.setState({text: e.target.value});
  },

  handleSubmit: function(e) {
    e.preventDefault();
    var newText = this.state.text.trim();
    if (newText == '') return;
    var nextText = '';
    this.setState({text: nextText});

    // trigger action on store
    actions.addItem(newText);
  },

  render: function() {
    return (
      <div>
        <h3>TODO</h3>
        <TodoList items={this.state.items} />
        <form onSubmit={this.handleSubmit}>
          <input onChange={this.onChange} value={this.state.text} />
          <button>{'Add #' + (this.state.items.length + 1)}</button>
        </form>
      </div>
    );
  }
});

module.exports = RefluxTodoApp;