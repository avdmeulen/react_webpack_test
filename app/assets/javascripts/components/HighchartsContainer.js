var React = require('react');
var Highcharts = require('react-highcharts');

var HighchartsContainer = React.createClass({
  getDefaultProps: function() {
    return {
      title: {
        text: 'Indicator Name komt hierzo',
        x: -20 //center
      },
      xAxis: {
        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
          'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
      },
      yAxis: {
        title: {
          text: 'Euro'
        },
        plotLines: [{
          value: 0,
          width: 1,
          color: '#808080'
        }]
      },
      tooltip: {
        valueSuffix: 'Euro'
      },
      legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'middle',
        borderWidth: 0
      },
      // series: this.props.model //..rest of Highcharts code
    };
  },

  render: function() {
    return <Highcharts config={this.props} series={this.props.series} />;
  }
});

module.exports = HighchartsContainer;
