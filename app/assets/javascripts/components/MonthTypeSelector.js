var React = require('react/addons');
var Reflux = require('reflux');
var SelectionActions = require('../actions/SelectionActions');
var SelectionStore = require('../stores/SelectionStore');

var MonthTypeSelector = React.createClass({
  mixins: [
    Reflux.connect(SelectionStore, 'selection')
  ],

  propTypes: {
    allOptions: React.PropTypes.array
  },

  getDefaultProps: function() {
    return {
      allOptions: [
        { value: 'm', label: 'Maandelijks' },
        { value: 'c', label: 'Jaarlijks' },
        { value: 'cc', label: 'Cumulatief' }
      ]
    };
  },
  
  handleChange: function(event) {
    // trigger action on store
    SelectionActions.updateSelection({monthType: event.target.value});
  },
  
  render: function() {
    var self = this;
    var options = _.filter(this.props.allOptions, function(option) {
      return self.props.monthTypes.indexOf(option.value) >= 0;
    }).map(function(option) {
      return <option key={option.value} value={option.value}>{option.label}</option>;
    });
    
    return (
      <select ref="monthType" name="monthType" value={this.state.selection.monthType} onChange={this.handleChange}>
        {options}
      </select>
    );
  }
});

module.exports = MonthTypeSelector;
