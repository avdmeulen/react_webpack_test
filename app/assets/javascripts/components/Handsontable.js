var React = require('react/addons');


var HandsOnTable = React.createClass({
  data: null,

  installRTE: function () {
    var el = this.getDOMNode();
    
    // this.rte = $(el).handsontable({
    this.rte = new Handsontable(el, {
      data: this.props.data || null,
      width: 'auto',
      height: 200,
      stretchH: 'all',
      rowHeaders: false,
      colHeaders: this.props.headers,
      minSpareRows: 1,
      currentRowClassName: 'currentRow',
      currentColClassName: 'currentCol',
      afterChange: this.handleChange
    });

    // from bootstrap: 'table-bordered table-striped table-hover'
    $(el).find('table').addClass('table-hover');
  },

  componentDidMount: function () {
    if (!this.rte) {
      this.installRTE();
    }
  },

  _syncChanges: function (input, changes) {
    // ex. [row, col, old, new]
    // ex. [3, 2, "ice", "water"]
    var output = input;
    for (var i in changes) {
      var change = changes[i];

      if (!output[change[0]]) {
        output[change[0]] = [];
      }
      output[change[0]][change[1]] = change[3];
    }
    return output;
  },

  handleChange: function (changes, source) {
    // console.log('change', changes, source);
    if (changes) {
      this.data = this._syncChanges(this.props.data, changes);
      this.props.onInputUpdate(this.data, changes);
    }
  },

  componentWillReceiveProps: function (nextProps) {
    if (!this.rte) {
      this.installRTE();
    }
    // console.log(nextProps.data, this.data)
    if (nextProps.data !== this.data) {
      // TODO: do this only sparingly under certain conditions
      // this might have been updated from the editor itself.
      // console.log(this.rte)
      this.rte.loadData(nextProps.data);
    }
  },

  render: function () {
    return (
      <div className={this.props.className + ' editor m-sheet--input'}></div>
    );
  }
});

module.exports = HandsOnTable;