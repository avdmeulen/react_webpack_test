var React = require('react/addons');
var Reflux = require('reflux');
var ProductStore = require('../stores/ProductStore');
var SelectionActions = require('../actions/SelectionActions');
var SelectionStore = require('../stores/SelectionStore');
var Bootstrap = require('react-bootstrap');
var classnames = require('classnames')

// This will display a show/hide button and some more info
// The children of this component will be shown/hidden accordingly.
var CollapsableParagraph = React.createClass({
  mixins: [
    Bootstrap.CollapsableMixin
  ],

  getCollapsableDOMNode: function() {
    return this.refs.panel.getDOMNode();
  },

  getCollapsableDimensionValue: function() {
    return this.refs.panel.getDOMNode().scrollHeight;
  },

  onHandleToggle: function(e) {
    e.preventDefault();
    this.setState({expanded:!this.state.expanded});
  },

  getFilterStatusText: function() {
    var filterStatus = "Per " + this.props.selection.dimension;
    var dimStatus = _.reject(_.map(this.props.selection.elements, function(element, dimension) {
      if (element == 'all' || element == '1') {
        return ''
      } else {
        return dimension + ": " + element
      }
    }), function(el) {
      return el === '';
    }).join(', ');
    if (dimStatus.length > 0) {
      filterStatus = filterStatus + ", " + dimStatus;
    }

    return filterStatus;
  },

  render: function(){
    let styles = this.getCollapsableClassSet();
    let text = this.isExpanded() ? 'Hide' : 'Show';
    return (
      <div>
        <Bootstrap.Button onClick={this.onHandleToggle}>{text} Filter</Bootstrap.Button>
        <span className="pull-right">{this.getFilterStatusText()}</span>
        <div ref='panel' className={classnames(styles)}>
          {this.props.children}
        </div>
      </div>
    );
  }
});

// The left part of this main component with a list of dimensions
var Focus = React.createClass({
  getDefaultProps: function() {
    return { dimension: 'sources' };
  },

  handleSelect: function(selectedKey) {
    // reset the selected element of the selected dimension to 'all'
    var changes = { dimension: selectedKey, elements: {} };
    changes.elements[selectedKey] = (selectedKey == 'sources' ? 'all' : '1');
    SelectionActions.updateSelection(changes);
  },

  render: function() {
    return (
      <Bootstrap.Nav bsStyle='pills' className="focusDimension" stacked activeKey={this.props.dimension} onSelect={this.handleSelect}>
        <Bootstrap.NavItem eventKey={'sources'} title='Overzicht'>Overzicht</Bootstrap.NavItem>
        <Bootstrap.NavItem eventKey={'departments'} title='Per afdeling'>Per afdeling</Bootstrap.NavItem>
        <Bootstrap.NavItem eventKey={'projects'} title='Per project'>Per project</Bootstrap.NavItem>
        <Bootstrap.NavItem eventKey={'products'} title='Per product'>Per product</Bootstrap.NavItem>
        <Bootstrap.NavItem eventKey={'relations'} title='Per relatie'>Per relatie</Bootstrap.NavItem>
      </Bootstrap.Nav>
    );
  }
});

// An inner row of the right part of the main component, displaying the name
// of the dimension, a checkbox which if selected displays a dropdown with elements.
var DimensionRow = React.createClass({
  getInitialState: function() {
    return {
      showDropdown: false 
    };
  },
  handleChange: function(event) {
    // change the selected element of the current dimension
    var changes = { elements: {} };
    changes.elements[this.props.dimension] = event.target.value;
    SelectionActions.updateSelection(changes);
  },

  toggleViewSelect: function(event) {
    var newState = {showDropdown: this.refs.checkbox.getDOMNode().checked};
    if (!newState.showDropdown) {
      // if unchecked, reset element to 'all'
      var changes = { elements: {} };
      changes.elements[this.props.dimension] = this.props.dimension == 'sources' ? 'all' : '1';
      SelectionActions.updateSelection(changes);
    }
    this.setState(newState);
  },

  componentWillReceiveProps: function(nextProps) {
    if (nextProps.selectedDimension === this.props.dimension) {
      this.setState({showDropdown: false})
    }
  },

  shouldComponentUpdate: function(nextProps, nextState) {
    // Prevent re-rendering when there are no interesting changes for this row
    return (nextProps.selectedDimension != this.props.selectedDimension && (nextProps.selectedDimension == this.props.dimension || this.props.selectedDimension == this.props.dimension)) || 
      nextProps.selectedElement != this.props.selectedElement ||
      nextState.showDropdown != this.state.showDropdown ||
      !(_.isEqual(nextProps.elements, this.props.elements));
  },

  render: function() {
    var options = this.props.elements.map(function(element) {
      return <option key={element.id} value={element.id}>{element.name}</option>;
    });
    var isSelected = this.props.dimension === this.props.selectedDimension;
    var firstColumn = <span>&nbsp;</span>;
    if (!isSelected) {
      firstColumn = <input type="checkbox" ref="checkbox" onChange={this.toggleViewSelect}></input>;
    }
    return (
      <tr>
        <td>{firstColumn}</td>
        <td>{this.props.label}</td>
        <td>
          <select className={this.state.showDropdown ? 'show' : 'hidden'} ref={this.props.dimension} 
            value={this.props.selectedElement} onChange={this.handleChange}>
            {options}
          </select>
        </td>
      </tr>
    );
  }
});

// The list with dimensions in the right part of this main element.
var DimensionList = React.createClass({
  getDefaultProps: function() {
    return { name: '', elements: [] };
  },

  render: function() {
    return (
      <table className="table">
        <colgroup>
          <col style={{width: 20 + 'px'}}/>
          <col style={{width: 80 + 'px'}}/>
          <col style={{width: 99 + '%', paddingLeft: 8 + 'px'}}/>
        </colgroup>
        <tbody>
          {this.props.children}
        </tbody>
      </table>
    );
  }
});

// The main component
var DimensionSelector = React.createClass({
  mixins: [
    Reflux.connect(ProductStore, 'products'), 
    Reflux.connect(SelectionStore, 'selection')
  ],

  render: function() {
    var availableSources = [
      { id: 'all', name: 'Overzicht' },
      { id: 'realisation', name: 'Realisatie' },
      { id: 'forecast', name: 'Forecast' },
      { id: 'ambition', name: 'Ambitie' }
    ];
    var availableDummies = [
      { id: '1', name: 'Alle' },
      { id: '2', name: 'Geen' },
      { id: '3', name: 'ABC' },
      { id: '4', name: 'XYZ' }
    ];
    return (
      <div>
        <CollapsableParagraph selection={this.state.selection}>
          <div className="row">
            <div className="col-md-4">
              <h4>Bekijk</h4>
              <Focus dimension={this.state.selection.dimension} />
            </div>
            <div className="col-md-8">
              <h4>Filter op</h4>
              <DimensionList dimension={this.state.selection.dimension}>
                <DimensionRow ref="sources" label="Type" dimension="sources" 
                  elements={availableSources}
                  selectedDimension={this.state.selection.dimension}
                  selectedElement={this.state.selection.elements.sources} />
                <DimensionRow ref="departments" label="Afdelingen" dimension="departments" 
                  elements={availableDummies}
                  selectedDimension={this.state.selection.dimension}
                  selectedElement={this.state.selection.elements.departments} />
                <DimensionRow ref="projects" label="Projecten" dimension="projects" 
                  elements={availableDummies}
                  selectedDimension={this.state.selection.dimension}
                  selectedElement={this.state.selection.elements.projects} />
                <DimensionRow ref="products" label="Producten" dimension="products" 
                  elements={this.state.products}
                  selectedDimension={this.state.selection.dimension}
                  selectedElement={this.state.selection.elements.products} />
                <DimensionRow ref="relations" label="Relaties" dimension="relations" 
                  elements={availableDummies}
                  selectedDimension={this.state.selection.dimension}
                  selectedElement={this.state.selection.elements.relations} />
              </DimensionList>
            </div>
          </div>
        </CollapsableParagraph>
      </div>
    )
  } 
});

module.exports = DimensionSelector;