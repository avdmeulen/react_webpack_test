var React = require('react/addons');
var Reflux = require('reflux');
var SelectionActions = require('../actions/SelectionActions');
var ProductActions = require('../actions/ProductActions');
var ProductStore = require('../stores/ProductStore');
var SelectionStore = require('../stores/SelectionStore');
var FinancialActions = require('../actions/FinancialActions');
var FinancialStore = require('../stores/FakeFinancialStore');
var MonthTypeSelector = require('./MonthTypeSelector');
var DimensionSelector = require('./DimensionSelector');
var MutationsBox = require('./MutationsBox');
// var HighchartsBar = require('./HighchartsBar');
var HighchartsContainer = require('./HighchartsContainer');
var Alert = require('react-bootstrap').Alert;
var HandsOnTable = require('../components/Handsontable');

var IndicatorApp = React.createClass({
  mixins: [
    Reflux.connect(ProductStore, 'products'), 
    Reflux.connect(SelectionStore, 'selection'),
    Reflux.connect(FinancialStore, 'series')
  ],

  propTypes: {
    indicatorId: React.PropTypes.number,
    monthTypes: React.PropTypes.array,
    selection: React.PropTypes.object
  },

  componentWillMount: function() {
    // initialize store with selection properties
    SelectionActions.updateSelection(this.props.selection);

    // Load products
    FinancialActions(this.state.selection);
    ProductActions();
  },

  handleInputUpdate: function(update, changes) {
    console.log('Handle HandsOnTable input update', update, changes);
  },

  render: function() {
    var handsonData =  [];
    // var handsonData = [['Column A', 'Column B', 'Column C']];
    this.state.products.forEach(function(product, index, array) {
      handsonData.push([product.code, product.id, product.name])
    });
    console.log(handsonData);

    return (
      <div>
        {this.props.indicatorId}
        <MonthTypeSelector monthTypes={this.props.monthTypes} />
        <DimensionSelector />
        
        <HighchartsContainer series={this.state.series} />
        <MutationsBox />
        <HandsOnTable
            headers={['Code', 'ID', 'Name']}
            data={handsonData} //{this.props.item.input ? this.props.item.input : ''}
            onInputUpdate={this.handleInputUpdate}
            // key={this.props.item.$id}
            className="" />
        <Alert><strong>Selection: </strong><pre>{JSON.stringify(this.state.selection, null, 4)}</pre></Alert>
      </div>
    );
  }
});

module.exports = IndicatorApp;
