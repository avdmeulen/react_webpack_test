# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

products = Product.create([
  { name: 'Alle', code: '' },
  { name: 'Geen', code: '' },
  { name: 'Hamer', code: '0001' },
  { name: 'Zaag', code: '0002' },
  { name: 'Spijker', code: '1001' },
  { name: 'Plank', code: '1002' }
])